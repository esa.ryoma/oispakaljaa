<?php

$kaikki_tuotteet = array("Kalja", "Viini", "Siideri", "Lonkero", "Shotti");
$kaikki_palvelut = array("Bilis", "Lautapelit", "Jukeboxi", "Karaoke", "Keittio");
// Tallennetaan muuttujat, jotka tulivat httpRequestin mukana
$tuotteet = [];
$palvelut = [];
$alueet = [];

foreach ($_REQUEST as $key => $value) // Jokainen key=value pari httpRequestista käydään läpi esim. beer=5 ja tallennetaan loopin ajaksi $key ja $value -muuttujiin
    if (in_array($key, $kaikki_tuotteet)) // Jos kaikki tuotteet pitää sisällään nykyisen $keyn (esim. kalja)
        $tuotteet[$key] = $value; // asetetaan $key-$value-pari uuteen listaan $tuotteet joita tutkitaan myöhemmin
    elseif (in_array($key, $kaikki_palvelut))
        switch($value) {
            case "true":
                $palvelut[$key] = 1;
                break;
            case "false":
                $palvelut[$key] = 0;
        }



$hakuehdot = array_merge($tuotteet, $palvelut); // Yksi iso hakuehto-taulukko: ["Kalja" => 3.5, "Bilis" => true]

if (array_key_exists("Alue", $_REQUEST)) { // Jos haussa oli mukana alueita, tallennetaan ne taulukkoon $alueet = [alue1, alue2, alue3...]
    $alueet = $_GET["Alue"];
}

//foreach ($hakuehdot as $key => $value) {
//    echo $key . "=>" . $value;
//}
//foreach ($alueet as $value) {
//    echo $value;
//}


$servername = "localhost";
$username = "testuser";
$password = "123456";

// Create connection
$conn = new mysqli($servername, $username, $password);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
//echo "Connected successfully";

// Which database to use
$db_selected =  $conn->select_db("oispakaljaa");
if (!$db_selected) {
    echo 'Cant use : '.$db_selected.' error:'. mysqli()->error;
}
else
    //echo "\nCan use database $db_selected";


// Hakukysely
// Kuinka monta hakuehtoa meillä on?
    //$hakuehtoja = count($tuotteet) + count($palvelut);
/* POIS KÄYTÖSTÄ TESTAUKSEN AJAKSI JA KESKEN
// Tehdään prepared statement, jossa tietty määrä tuote-ehtoja ja tietty määrä palvelut-ehtoja
foreach ($tuotteet as $tuote => $arvo)
    $stmt = $conn->prepare("SELECT * FROM baari INNER JOIN juomavalikoima, varustelu ON baari.id=juomavalikoima.id=varustelu.id
    WHERE ");
    for ($i = 0; $i < count($hakuehtoja); $i++)
$stmt->bind_param("sss", $firstname, $lastname, $email);
*/


/* TODO PREPARED STATEMENT INSTEAD OF STRAIGHT SQL QUERY DOES NOT WORK
$sql = "SELECT * FROM baari INNER JOIN juomavalikoima, varustelu ON baari.id=juomavalikoima.id AND baari.id = varustelu.id
WHERE ? = ?";
$keys = array_keys($tuotteet);
if ($query = $conn->prepare($sql)) {
    $query->bind_param('sd', $keys[0], $tuotteet[$keys[0]]);
    $result = $query->execute();
    echo '<br/>Total results: ' . $result->num_rows;
} else {
    $error = $conn->errno . ' ' . $conn->error;
    echo $error; // 1054 Unknown column 'foo' in 'field list'
}
*/

$sql = "SELECT * FROM baari, juomavalikoima, varustelu WHERE baari.Id = juomavalikoima.Id = varustelu.Id AND ";

// HAKUEHDOT FEAT. ALUEET NEW HOTNESS
// Onko annettu tuotteita tai palveluja hakuehdoiksi?
if (count($hakuehdot) > 0) {
    $tempVar = 0;
    foreach ($hakuehdot as $key => $value) {
        if (in_array($key, $kaikki_tuotteet)) {// Jos avain on tuotteen nimi
            //echo $key . "=>" . $value;
            $sql .= $key . "<=" . $value; // haetaan yhtää suuria tai pienempiä hintoja
        } elseif (in_array($key, $kaikki_palvelut)) {// Jos avain on palvelun nimi
            //echo $key . "=>" . $value;
            $sql .= $key . "=" . $value; // haetaan tarkkaa vastaavuutta 0 tai 1
        }

        $tempVar++;
        if ($tempVar <= sizeof($hakuehdot)-1) {
            $sql .= " AND "; // Jos EI olla hakuehtojen viimeisessä ehdossa, lisätään sql-lauseeseen AND
        }

    }
    // Onko tuotteiden/palveluiden lisäksi haussa myös alue?
    if (sizeof($alueet) > 0){ // Jos alueita on annettu
        for ($i = 0; $i < count($alueet); $i++) {
            if ($i == 0) { // Jos ollaan käsittelemässä $alueet-taulukon ensimmäistä kohtaa
                $sql .= " AND (Alue=" . $alueet[$i]; // Lisätään lause sql:ään, (-merkki tärkeä, koska se ryhmittää ehdot ...AND (Alue=1 OR Alue=2)
            } else { // Jos käsitellään muuta kuin ensimmäistä Aluetta.
                $sql .= " OR Alue=" . $alueet[$i]; // Lisätään lause sql:ään
            }
        }
        $sql .= ")"; // viimeinen sulku lausekkeeseen ...OR Alue=2)
    }

} else { // Pelkästään alueita annettu hakuehdoiksi
    if (sizeof($alueet) > 0){ // Jos alueita on annettu
        for ($i = 0; $i < count($alueet); $i++) {
            if ($i == 0) { // Jos ollaan käsittelemässä $alueet-taulukon ensimmäistä kohtaa
                $sql .= "(Alue=" . $alueet[$i]; // Lisätään lause sql:ään
            } else { // Jos käsitellään muuta kuin ensimmäistä Aluetta.
                $sql .= " OR Alue=" . $alueet[$i]; // Lisätään lause sql:ään
            }
        }
    $sql .= ")"; // viimeinen sulku lausekkeeseen ...OR Alue=2)
    }

}


// HAKUEHDOT ILMAN ALUETTA OLD AND BUSTED
// Haku koko hakuehto-taulukosta $hakuehdot
/*
$tempVar = 0;
foreach ($hakuehdot as $key => $value) {
    if (in_array($key, $kaikki_tuotteet)) {// Jos avain on tuotteen nimi
        //echo $key . "=>" . $value;
        $sql .= $key . "<=" . $value; // haetaan yhtää suuria tai pienempiä hintoja
    } elseif (in_array($key, $kaikki_palvelut)) {// Jos avain on palvelun nimi
        //echo $key . "=>" . $value;
        $sql .= $key . "=" . $value; // haetaan tarkkaa vastaavuutta 0 tai 1
    }

    $tempVar++;
    if ($tempVar <= sizeof($hakuehdot)-1) {
        $sql .= " AND "; // Jos EI olla hakuehtojen viimeisessä ehdossa, lisätään sql-lauseeseen AND
    }

// Lisätään hakuun Alue, jos sellainen on annettu
if (sizeof($alueet) > 0){ // Jos alueita on annettu
    for ($i = 0; $i < count($alueet); $i++) {
        if ($i == 0) { // Jos ollaan käsittelemässä $alueet-taulukon ensimmäistä kohtaa
            $sql .= "AND (Alue=" . $alueet[$i]; // Lisätään lause sql:ään, (-merkki tärkeä, koska se ryhmittää ehdot ...AND (Alue=1 OR Alue=2)
        } else { // Jos käsitellään muuta kuin ensimmäistä Aluetta.
            $sql .= "OR Alue=" . $alueet[$i]; // Lisätään lause sql:ään
        }
    }
    $sql .= ")"; // viimeinen sulku lausekkeeseen ...OR Alue=2)
}
*/

// viimeinen ;-merkki sql-lauseen loppuun
$sql .= ";";
//echo $sql;

// Haku pelkistä tuotteista
/*
$tempVar = 0;
foreach ($tuotteet as $key => $value)
    $sql .= $key . "<=" . $value;
    if ($tempVar < sizeof($tuotteet)-1)
        $sql .= " AND ";
    $tempVar++;

*/


if(!$result = $conn->query($sql)){
    die('There was an error running the query [' . $conn->error . ']');
}
//echo '<br/>Total results: ' . $result->num_rows;

$rivitaulukko = [];
while ($row = $result->fetch_assoc())
    array_push($rivitaulukko, $row);

// Jos tulosrivejä löytyy, tehdään json-encode, muuten palautetaan null
echo count($rivitaulukko) > 0 ? json_encode($rivitaulukko) : null;





