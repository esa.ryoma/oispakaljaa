var slider = document.getElementById("myRange");
var output = document.getElementById("demo");
var area = 0;
var url = 'getKaljav2.php?';
let httpRequest;
output.innerHTML = slider.value + "€"; // Display the default slider value

// Update the current slider value (each time you drag the slider handle)
slider.oninput = function() {
    output.innerHTML = this.value + "€";
};

function hesari() {
    console.log('hesari');
    area = 1;
    //alue 1
};

function linjat() {
    console.log('linja');
    area = 2;
    //alue 2
};

function karhupuisto() {
    console.log('karhupuisto');
    area = 3;
    //alue 3
};

function harju() {
    console.log('harju');
    area = 4;
    // alue 4
};

function makeRequest() {
    if (window.XMLHttpRequest) { // Mozilla, Safari, ...
        httpRequest = new XMLHttpRequest();
    } else if (window.ActiveXObject) { // IE
        try {
            httpRequest = new ActiveXObject("Msxml2.XMLHTTP");
        }
        catch (e) {
            try {
                httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
            }
            catch (e) {}
        }
    }

    if (!httpRequest) {
        alert('Giving up :( Cannot create an XMLHTTP instance');
        return false;
    }

    // set a callback function for when the httpRequest completes
    httpRequest.onreadystatechange = getResults;


    // Go through all the checkboxes and save the checked id's and values into a table.
    let tuotteet = ["kalja", "viini", "siideri", "lonkero", "shotti"];
    let palvelut = ["bilis", "lautapelit", "jukebox", "karaoke", "keittio"]
    let kiinnostavatTuotteet = {};
    let kiinnostavatPalvelut = {};

    for (let i = 0; i < tuotteet.length; i++) {
        let input = document.getElementById(tuotteet[i]);
        if (input.checked === true) {
            kiinnostavatTuotteet[tuotteet[i]] = slider.value; // Jos checkbox on chekattu, lisätään se tuotelistaobjektiin {tuote:hinta}
        }
    }

    for (let i = 0; i < palvelut.length; i++) {
        let input = document.getElementById(palvelut[i]);
        if (input.checked === true) kiinnostavatPalvelut[palvelut[i]] = true; // Jos checkbox on chekattu, lisätään se kiinnostavien palvelujen arrayhin nimellään
    }

    console.log(kiinnostavatTuotteet);
    console.log(kiinnostavatPalvelut);
    // Tässä vaiheessa chekatut tuotteet objektissa kiinnostavatTuotteet {tuote1:hinta, tuote2:hinta...}
    // Chekatut palvelut objektissa kiinnostavatPalvelut {palvelu1:true,palvelu2:true...}

    let query = url + "?";
    for (let key in kiinnostavatTuotteet) {
        query += key + "=" + kiinnostavatTuotteet[key]; // lisätään queryyn tuotelistan key=value pari
    }

    for (let key in kiinnostavatPalvelut) {
        query += key + "=" + true; // lisätään queryyn palvelulistan key=true pari
    }

    console.log(query);


    // now do the actual AJAX request
    httpRequest.open('GET', 'getKaljav2.php', true); // (GET/POST/??, URL, TRUE=async / FALSE=sync)
    // https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest/open

    httpRequest.send();
}

// the callback
// this will be run when the request completes (see above)
function getResults() {
    if (httpRequest.readyState === 4) {
        if (httpRequest.status === 200) {
            alert(httpRequest.responseText);
        } else {
            alert('There was a problem with the request.');
        }
    } else {
        console.log(httpRequest.readyState);
    }
}